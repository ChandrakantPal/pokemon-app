import * as React from "react"
import classNames from "classnames"

const Button: React.FunctionComponent<
  React.PropsWithChildren<
    React.DetailedHTMLProps<
      React.ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >
  >
> = ({ children, className, ...rest }) => {
  return (
    <button
      className={classNames(
        className,
        "cursor-pointer text-center bg-[#2D2F36] text-[#EEECEC] font-semibold text-base rounded-lg hover:bg-[#F2C94C] hover:text-[#2D2F36]"
      )}
      {...rest}
    >
      {children}
    </button>
  )
}

export default Button
