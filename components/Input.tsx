import * as React from "react"

const Input: React.FunctionComponent<
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >
> = ({ className, ...rest }) => {
  return (
    <input
      {...rest}
      className={`${className} bg-[#3F414B] rounded-md w-full outline-none focus:border-[#F2C94C] focus:outline-[#F2C94C] py-4 px-3 block text-[#8B8B8B] focus:text-white`}
    />
  )
}

export default Input
