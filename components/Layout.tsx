import classNames from "classnames"
import { useRouter } from "next/router"
import * as React from "react"

const Layout: React.FunctionComponent<
  React.PropsWithChildren<{ bg?: string }>
> = ({ children, bg }) => {
  const router = useRouter()
  return (
    <div
      className={classNames(
        "w-full min-h-screen flex  items-center justify-center",
        {
          ["bg-[#1C1D1F]"]: router.pathname === "/",
          ["bg-[#484D57]"]: router.pathname === "/list",
        }
      )}
    >
      {children}
    </div>
  )
}

export default Layout
