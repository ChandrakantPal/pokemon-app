import Image from "next/image"
import * as React from "react"

const ListItem: React.FunctionComponent<{
  image: string
  name: string
  number: string
  selectPokemon: () => void
}> = ({ image, number, name, selectPokemon }) => {
  return (
    <div
      className="bg-[#3F414B] flex items-center px-7 py-4 rounded-md mb-3.5 cursor-pointer"
      onClick={selectPokemon}
    >
      <div className="flex items-center">
        {image && name && (
          <div className="relative w-11 h-11">
            <Image
              src={image}
              alt={name}
              layout="fill"
              objectFit="cover"
              className="rounded-full"
            />
          </div>
        )}
        <p className="text-[#F2C94C] font-bold lg:text-lg mr-2.5 ml-7">
          {number}
        </p>
      </div>
      <p className="font-semibold text-center text-white lg:text-lg">{name}</p>
    </div>
  )
}

export default ListItem
