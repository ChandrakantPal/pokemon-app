import * as React from "react"

const Modal: React.FunctionComponent<
  React.PropsWithChildren<{ close: () => void }>
> = ({ children, close }) => {
  return (
    <div
      className="fixed top-0 bottom-0 left-0 right-0 z-10 flex items-center justify-center shadow-sm backdrop-blur"
      onClick={close}
    >
      <div className="flex items-center" onClick={(e) => e.stopPropagation()}>
        <main className="overflow-scroll bg-[#3B3E46] rounded-lg">
          {children}
        </main>
      </div>
    </div>
  )
}

export default Modal
