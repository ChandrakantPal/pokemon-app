import classNames from "classnames"
import * as React from "react"
import Button from "./Button"

interface PaginationProps {
  length: number
  currentIndex: number
  click: (index: number) => void
}

const Pagination: React.FunctionComponent<PaginationProps> = ({
  length,
  click,
  currentIndex,
}) => {
  const indexes = React.useMemo(() => Math.round(length / 38), [length])
  const indexArray: number[] = React.useMemo(() => [], [])
  React.useMemo(() => {
    for (let i = 0; i < indexes; i++) {
      indexArray.push(i + 1)
    }
  }, [indexArray, indexes])

  return (
    <div className="bg-[#1F1F1F] lg:rounded-bl-lg h-[10%] items-center flex justify-around md:justify-between md:px-2.5 px-4 flex-wrap">
      <div className="flex flex-wrap items-center">
        {indexArray.map((index) => (
          <Button
            key={index}
            className={classNames("px-2 py-1 mx-1.5", {
              ["bg-[#F2C94C]"]: index === currentIndex,
              ["text-[#2D2F36]"]: index === currentIndex,
            })}
            onClick={() => click(index)}
          >
            {index}
          </Button>
        ))}
      </div>
      <div className="flex flex-wrap items-center">
        <Button
          className="px-4 mx-1.5"
          onClick={() => {
            if (currentIndex === 1) {
              return
            }
            click(currentIndex - 1)
          }}
        >
          Prev
        </Button>
        <Button
          className="px-4"
          onClick={() => {
            if (currentIndex === indexes) {
              return
            }
            click(currentIndex + 1)
          }}
        >
          Next
        </Button>
      </div>
    </div>
  )
}

export default Pagination
