import Image from "next/image"
import * as React from "react"
import { Pokemon } from "../utils/types"
import Typography from "./Typography"

const PokemonDetail: React.FunctionComponent<{ pokemon: Pokemon }> = ({
  pokemon,
}) => {
  return (
    <div className="rounded-lg h-4/5 border-[#2D2F36]">
      <header className="flex items-center justify-between border-b-[#2D2F36] border-b-[1px] h-1/5 px-4">
        <p className="text-[#EDEDED] font-semibold text-lg md:text-xl xl:text-3xl">
          {pokemon.name}
        </p>
        <p className="text-[#F2C94C] font-semibold text-lg md:text-xl xl:text-3xl">
          #{pokemon.number}
        </p>
      </header>
      {/* Card */}
      <div className="p-2 mx-auto md:p-10 h-4/5">
        {/* Image */}
        <div className="relative w-24 h-24 mx-auto overflow-hidden bg-white md:w-64 md:h-64 lg:w-72 lg:h-72 xl:w-96 xl:96 rounded-xl">
          <Image
            src={pokemon.image}
            alt={pokemon.name}
            layout="fill" // required
            objectFit="contain"
            className="rounded-xl brightness-75 contrast-200"
          />
        </div>
        {/* details */}
        <div className="mx-auto mt-2 text-left md:w-2/3">
          {/* type, weight, height */}
          <Typography className="text-[#F2C94C]">
            <span className="mr-1">{pokemon.classification}</span>
            <span className="mr-1">{pokemon.weight?.minimum} </span>
            <span>{pokemon.height?.minimum}</span>
          </Typography>
          {/* HP */}
          <Typography className="text-[#F2C94C] mt-2">
            <span className="mr-1">⚡️</span>
            <span>{pokemon.maxHP}</span>
          </Typography>
          {/* Attacks */}
          <Typography className="text-[#EDEDED] flex justify-between items-center mt-2">
            <span>Attacks</span>
            <span>Damage</span>
          </Typography>
          {pokemon.attacks?.special?.map((item, i: number) => (
            <Typography
              className="text-[#F2C94C] flex justify-between items-cente mt-1"
              key={i}
            >
              <span>★ {item?.name}</span>
              <span>{item?.damage}</span>
            </Typography>
          ))}
          {/* Weakness */}
          <Typography className="text-[#EDEDED] mt-2">
            Weakness -{" "}
            {pokemon.weaknesses?.map((item, i: number) => (
              <span key={item} className="text-[#F2C94C] mr-1">
                {item}{" "}
              </span>
            ))}
          </Typography>
        </div>
      </div>
    </div>
  )
}

export default PokemonDetail
