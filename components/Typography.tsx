import classNames from "classnames"
import * as React from "react"

const Typography: React.FunctionComponent<
  React.PropsWithChildren<
    React.DetailedHTMLProps<
      React.HTMLAttributes<HTMLParagraphElement>,
      HTMLParagraphElement
    >
  >
> = ({ children, className, ...rest }) => {
  return (
    <p
      className={classNames(
        "font-semibold text-sm sm:text-base md:text-lg lg:text-lg xl:text-xl 2xl:text-2xl",
        className
      )}
      {...rest}
    >
      {children}
    </p>
  )
}

export default Typography
