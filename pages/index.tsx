import * as React from "react"
import type { NextPage } from "next"
import Head from "next/head"
import { useRouter } from "next/router"
import Input from "../components/Input"
import Layout from "../components/Layout"

enum Cred {
  email = "pokemon@pokemon.com",
  password = "Pokemon#123",
}

const Home: NextPage = () => {
  const [email, setEmail] = React.useState("")
  const [password, setPassword] = React.useState("")
  const [emailError, setEmailError] = React.useState("")
  const [passwordError, setPasswordError] = React.useState("")

  const router = useRouter()

  const validateEmail = () =>
    /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(email)

  const submitHandler = (
    event:
      | React.MouseEvent<HTMLButtonElement, MouseEvent>
      | React.FormEvent<HTMLFormElement>
  ) => {
    event.preventDefault()
    if (email === "" && password === "") {
      setEmailError("Email is required")
      setPasswordError("Password is required")
      return
    }
    if (email === "") {
      setEmailError("Email is required")
      setPasswordError("")
      return
    }
    if (password === "") {
      setPasswordError("Password is required")
      setEmailError("")
      return
    }
    if (!validateEmail()) {
      setEmailError("Invalid Email")
      setPasswordError("")
      return
    }
    if (email !== Cred.email && password !== Cred.password) {
      setEmailError("Invalid Email or Password")
      setPasswordError("Invalid Email or Password")
      return
    }
    setEmail("")
    setPassword("")
    setEmailError("")
    setPasswordError("")
    router.push("/list")
  }
  return (
    <Layout bg="#1C1D1F">
      <Head>
        <title>Pokémon Login</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="2xl:w-[508px] xl:w-[508px] md:w-1/2 lg:w-1/2 w-4/5 mx-auto">
        <form
          className="w-full bg-[#2D2F36] rounded-lg sm:p-6 p-5 md:p-16 lg:p-16 xl:p-16 2xl:p-16"
          onSubmit={submitHandler}
        >
          <Input
            placeholder="Email"
            type="email"
            value={email}
            required
            tabIndex={-1}
            onChange={(e) => setEmail(e.target.value)}
          />
          {emailError && <small className="text-red-400">{emailError}</small>}
          <Input
            placeholder="Password"
            type="password"
            value={password}
            className="mt-7"
            required
            tabIndex={-1}
            onChange={(e) => setPassword(e.target.value)}
          />
          {passwordError && (
            <small className="text-red-400">{passwordError}</small>
          )}
          <button
            className="bg-[#F2C94C] w-full text-center py-4 rounded-md text-white font-bold text-lg mt-11"
            onClick={submitHandler}
          >
            LOGIN
          </button>
          <div className="flex items-center justify-center w-full mt-2"></div>
        </form>
      </main>
    </Layout>
  )
}

export default Home
