import * as React from "react"
import Head from "next/head"
import Button from "../components/Button"
import Layout from "../components/Layout"
import ListItem from "../components/ListItem"
import Pagination from "../components/Pagination"
import Image from "next/image"
import PokemonDetail from "../components/PokemonDetail"
import { useQuery } from "@apollo/client"
import { POKEMON } from "../graphql/pokemons"
import { Pokemon } from "../utils/types"
import Modal from "../components/Modal"
import Typography from "../components/Typography"

interface Data {
  pokemons: Pokemon[]
}

const List = () => {
  const { data, fetchMore, error, loading } = useQuery<Data>(POKEMON, {
    variables: {
      first: 150,
    },
    fetchPolicy: "network-only",
  })
  const [selectedPokemon, setSelectedPokemon] = React.useState<Pokemon | null>(
    null
  )
  const [currentIndex, setCurrentIndex] = React.useState(1)
  const [start, setStart] = React.useState(0)
  const [end, setEnd] = React.useState(38)

  const paginationClickHandler = (index: number) => {
    setCurrentIndex(index)
    if (index === 1) {
      setStart(0)
    } else {
      setStart((index - 1) * 38)
    }
    setEnd(index * 38)
  }
  const pokemons = data?.pokemons?.slice(start, end)

  const pokemonSelectHandler = React.useCallback((pokemon: Pokemon) => {
    setSelectedPokemon(pokemon)
  }, [])
  if (loading) {
    return (
      <Layout>
        <div className="relative w-24 h-24">
          <Image
            src="/pokemonball.png"
            alt="loading"
            layout="fill"
            objectFit="cover"
          />
        </div>
      </Layout>
    )
  }
  return (
    <>
      <Layout bg={"#484D57"}>
        <Head>
          <title>Pokémons</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main className="flex flex-col-reverse w-full h-screen md:flex-row md:h-[737px] 2xl:h-[1200px] xl:w-[1198px] md:mx-auto lg:w-3/4">
          <div className=" h-full bg-[#2D2F36] md:h-full md:w-2/5 xl:w-[508px] lg:rounded-tl-lg lg:rounded-bl-lg">
            <aside className="px-6 pt-4 overflow-y-scroll lg:pt-16 h-[90%] xl:px-14">
              {pokemons &&
                pokemons.map((pokemon) => (
                  <ListItem
                    key={pokemon.id}
                    number={pokemon.number}
                    name={pokemon.name}
                    image={pokemon.image}
                    selectPokemon={() => {
                      pokemonSelectHandler(pokemon)
                    }}
                  />
                ))}
            </aside>
            {data?.pokemons && (
              <Pagination
                length={data?.pokemons?.length}
                click={paginationClickHandler}
                currentIndex={currentIndex}
              />
            )}
          </div>
          <aside className="bg-[#3B3E46] hidden md:block md:h-full md:w-3/5 xl:w-[690px] lg:rounded-tr-lg lg:rounded-br-lg">
            {selectedPokemon ? (
              <PokemonDetail pokemon={selectedPokemon} />
            ) : (
              <div className="flex flex-col items-center justify-center w-full h-full">
                <div className="relative w-24 h-24">
                  <Image
                    src="/pokemonball.png"
                    alt="loading"
                    layout="fill"
                    objectFit="cover"
                  />
                </div>
                <Typography className="mt-2 text-center">
                  Select Pokemon
                </Typography>
              </div>
            )}
          </aside>
        </main>
      </Layout>
      <div className="md:hidden">
        {selectedPokemon && (
          <Modal
            close={() => {
              setSelectedPokemon(null)
            }}
          >
            <PokemonDetail pokemon={selectedPokemon} />
          </Modal>
        )}
      </div>
    </>
  )
}

export default List
