interface Attack {
  damage?: number
  name?: string
  type?: string
};
interface PokemonAttack {
  fast: Attack[]
  special: Attack[]
};

interface PokemonDimension {
  maximum?: string;
  minimum?: string;
};

export interface Pokemon {
  __typename: 'Pokemon';
  attacks: PokemonAttack;
  classification: string;
  fleeRate: number;
  height: PokemonDimension;
  id: string;
  image: string;
  maxCP: number
  maxHP: number
  name: string;
  number: string;
  resistant: string[];
  types: string[];
  weaknesses: string[];
  weight: PokemonDimension;
}